<?php
/**
 * Main class of module mj_sprzatacz.
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

class Mjpscleaner extends Module
{

    public $tables;

    //  Inicjalizacja
    public function __construct()
    {
        $this->name = 'mjpscleaner';
        $this->tab = 'others';
        $this->version = '1.01';
        $this->author = 'MAGES Michał Jendraszczyk';

        $this->bootstrap = true;
        $this->module_key = '067e79ab484b722c974e584ad6bd98b9';

        parent::__construct();

        $this->displayName = $this->l('Fast and simply accelerating Prestashop Cleaner');
        $this->description = $this->l('Really good module to removing a big statistics data from Prestashop what finally accelerators your shop.');

        $this->confirmUninstall = $this->l('Remove module?');
    }

    //  Instalacja
    public function install()
    {
        parent::install();

        Configuration::updateValue('mj_sprzatacz_cron', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php?token='.Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));
        Configuration::updateValue('mj_sprzatacz_pslog', 0);
        Configuration::updateValue('mj_sprzatacz_psconnections', 0);
        Configuration::updateValue('mj_sprzatacz_psconnections_source', 0);
        Configuration::updateValue('mj_sprzatacz_psguest', 0);
        Configuration::updateValue('mj_sprzatacz_pssearch_index', 0);
        Configuration::updateValue('mj_sprzatacz_cron_token', Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));

        return true;
    }

    // Deinstalacja
    public function uninstall()
    {
        return parent::uninstall();
    }

    // Budowanie formularza
    public function renderForm()
    {
        $fields_form = array();

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Cleaning settings'),
            ),
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Remove data from ' . _DB_PREFIX_ . 'log'),
                    'name' => 'mj_sprzatacz_pslog',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'mj_sprzatacz_pslog_on',
                            'value' => 1,
                            'label' => $this->l('Enabled'),
                        ),
                        array(
                            'id' => 'mj_sprzatacz_pslog_off',
                            'value' => 0,
                            'label' => $this->l('Disabled'),
                        ),
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Remove data from ' . _DB_PREFIX_ . 'connections_source'),
                    'name' => 'mj_sprzatacz_psconnections_source',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'mj_sprzatacz_psconnections_source_on',
                            'value' => 1,
                            'label' => $this->l('Enabled'),
                        ),
                        array(
                            'id' => 'mj_sprzatacz_psconnections_source_off',
                            'value' => 0,
                            'label' => $this->l('Disabled'),
                        ),
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Remove data from ' . _DB_PREFIX_ . 'connections'),
                    'name' => 'mj_sprzatacz_psconnections',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'mj_sprzatacz_psconnections_on',
                            'value' => 1,
                            'label' => $this->l('Enabled'),
                        ),
                        array(
                            'id' => 'mj_sprzatacz_psconnections_off',
                            'value' => 0,
                            'label' => $this->l('Disabled'),
                        ),
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Remove data from ' . _DB_PREFIX_ . 'guest'),
                    'name' => 'mj_sprzatacz_psguest',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'mj_sprzatacz_psguest_on',
                            'value' => 1,
                            'label' => $this->l('Enabled'),
                        ),
                        array(
                            'id' => 'mj_sprzatacz_psguest_off',
                            'value' => 0,
                            'label' => $this->l('Disabled'),
                        ),
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Remove data from ' . _DB_PREFIX_ . 'search_index'),
                    'name' => 'mj_sprzatacz_pssearch_index',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'mj_sprzatacz_pssearch_index_on',
                            'value' => 1,
                            'label' => $this->l('Enabled'),
                        ),
                        array(
                            'id' => 'mj_sprzatacz_pssearch_index_off',
                            'value' => 0,
                            'label' => $this->l('Disabled'),
                        ),
                    ),
                ),
            ),
                'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'config_save',
                'class' => 'btn btn-default pull-right',
            ),
        );

        $fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('CRON task'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('URL to CRON service'),
                    'desc' => $this->l('URL to run in CRON task manager'),
                    'size' => '5',
                    'name' => 'mj_sprzatacz_cron',
                    'disabled' => 'disabled',
                    'required' => false,
                ),
            ),
            'submit' => array(
                'title' => $this->l('Cleaning'),
                'class' => 'btn btn-default pull-right',
            ),
        );
        $form = new HelperForm();

        $form->token = Tools::getAdminTokenLite('AdminModules');
        $form->currentIndex = Tools::getHttpHost(true) . __PS_BASE_URI__ . basename(PS_ADMIN_DIR) . '/' . AdminController::$currentIndex . '&configure=' . $this->name . '&export=1';

        $form->tpl_vars['fields_value']['mj_sprzatacz_cron'] = Tools::getValue('mj_sprzatacz_cron', Configuration::get('mj_sprzatacz_cron'));
        $form->tpl_vars['fields_value']['mj_sprzatacz_pslog'] = Tools::getValue('mj_sprzatacz_pslog', Configuration::get('mj_sprzatacz_pslog'));
        $form->tpl_vars['fields_value']['mj_sprzatacz_psconnections_source'] = Tools::getValue('mj_sprzatacz_psconnections_source', Configuration::get('mj_sprzatacz_psconnections_source'));
        $form->tpl_vars['fields_value']['mj_sprzatacz_psconnections'] = Tools::getValue('mj_sprzatacz_psconnections', Configuration::get('mj_sprzatacz_psconnections'));
        $form->tpl_vars['fields_value']['mj_sprzatacz_psguest'] = Tools::getValue('mj_sprzatacz_psguest', Configuration::get('mj_sprzatacz_psguest'));
        $form->tpl_vars['fields_value']['mj_sprzatacz_pssearch_index'] = Tools::getValue('mj_sprzatacz_pssearch_index', Configuration::get('mj_sprzatacz_pssearch_index'));
        $form->tpl_vars['fields_value']['mj_sprzatacz_cron_token'] = Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php');

        return $form->generateForm($fields_form);
    }

    // Wyswietlenie contentu
    public function getContent()
    {
        return $this->postProcess() . $this->RenderForm();
    }

    public function postProcess()
    {
        if (Tools::isSubmit('config_save')) {
            Configuration::updateValue('mj_sprzatacz_cron', Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php?token='.Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));
            Configuration::updateValue('mj_sprzatacz_pslog', Tools::getValue('mj_sprzatacz_pslog'));
            Configuration::updateValue('mj_sprzatacz_psconnections_source', Tools::getValue('mj_sprzatacz_psconnections_source'));
            Configuration::updateValue('mj_sprzatacz_psconnections', Tools::getValue('mj_sprzatacz_psconnections'));
            Configuration::updateValue('mj_sprzatacz_psguest', Tools::getValue('mj_sprzatacz_psguest'));
            Configuration::updateValue('mj_sprzatacz_pssearch_index', Tools::getValue('mj_sprzatacz_pssearch_index'));
            Configuration::updateValue('mj_sprzatacz_cron_token', Tools::getAdminToken(Tools::getHttpHost(true) . __PS_BASE_URI__ . 'modules/' . $this->name . '/cron.php'));

            return $this->displayConfirmation('Saved changes!');
        }
        if (Tools::isSubmit('submitAddconfiguration')) :
            $this->cronWorker();

            $this->tables .= "Cleaned tables \n";

            return $this->displayConfirmation($this->tables);
        endif;
    }

    public function cronWorker()
    {
        // Ok zaczynamy porzątki
        //ps_connections_source
        //ps_search_index
        //ps_log
        //ps_connections
        //ps_guest

        $settings = array(
            'mj_sprzatacz_pslog',
            'mj_sprzatacz_psconnections_source',
            'mj_sprzatacz_psconnections',
            'mj_sprzatacz_psguest',
            'mj_sprzatacz_pssearch_index',
        );

        $cleaningTables = array(
            'log',
            'connections_source',
            'connections',
            'guest',
            'search_index',
        );

        foreach ($cleaningTables as $key => $table) {
            if (Configuration::get($settings[$key]) == 1) {
                $sql_select = 'SELECT * FROM ' . _DB_PREFIX_ . '' . $table;
                $result_count = Db::getInstance()->ExecuteS($sql_select, 1, 0);

                $sql_cleaning = 'DELETE FROM ' . _DB_PREFIX_ . '' . $table;

                Db::getInstance()->Execute($sql_cleaning, 1, 0);

                $this->tables .= 'Data from table ' . _DB_PREFIX_ . '' . $table . ' was removed (' . count($result_count) . " recodrs) \n";
            }
        }
        return 'Cleaning success!';
    }
}
