<?php
/**
 * File to run a CRON job
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
include_once 'mjpscleaner.php';

$celaningTables = new Mjpscleaner();

if (Configuration::get('mj_sprzatacz_cron_token') == Tools::getValue('token')) {
    $celaningTables->cronWorker();

    echo "\nOK";
} else {
    echo "\nWrong token!";
}
